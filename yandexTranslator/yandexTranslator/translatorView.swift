//
//  translatorView.swift
//  yandexTranslator
//
//  Created by Emir haktan Ozturk on 24/08/16.
//  Copyright © 2016 emirhaktan. All rights reserved.
//

import UIKit

class translatorView: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource {
    var translate = [TranslateLanguage]()
    var getLan = [SupportedLangs]()
    var pickerArray = NSArray()
    
    @IBOutlet var translateFrom: UITextField!
    @IBOutlet var translateTo: UITextField!
    @IBOutlet var translatedTextLabel: UILabel!
    @IBOutlet var textToTranslateTextField: UITextField!
    
    var picker = UIPickerView()
    var pickerTranslateFrom = UIPickerView()
    var pickerTranslateTo = UIPickerView()
    
    var translation = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        preparePickerView()

        self.translateFrom.inputView = pickerTranslateFrom
        self.translateTo.inputView = pickerTranslateTo
        
        callApiGetLang()
    }
    
    //method to prepare UIPickerView
    func preparePickerView(){
        pickerTranslateFrom.delegate = self
        pickerTranslateFrom.dataSource = self
        pickerTranslateTo.delegate = self
        pickerTranslateTo.dataSource = self
        
        pickerTranslateFrom.tag = 0
        pickerTranslateTo.tag = 1
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.Default
        toolBar.translucent = true
        toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(translatorView.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        //let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(translatorView.donePicker))
        
        toolBar.setItems([/*cancelButton,*/ spaceButton, doneButton], animated: false)
        toolBar.userInteractionEnabled = true

        self.translateFrom.inputAccessoryView = toolBar
        self.translateTo.inputAccessoryView = toolBar
    }
    
    
    // calls api for supported languages
    func callApiGetLang(){
        let finalUrlGetLang = storyboard.yandexURL+storyboard.apiUrlGetLang+storyboard.apiKey
        
        let api = APIManager()
        //Get Langs api
        api.getLangs(finalUrlGetLang, completion: showDataGetLangs)
    }
    // calls api to translate text
    func callApiTranslateText(link:String){
        let api = APIManager()
        api.translateText(link, completion: showTranslatedText)
    }
    
    //UIPickerView done button action
    func donePicker(){
        translateFrom.resignFirstResponder()
        translateTo.resignFirstResponder()
    }
    
    // Method to show translated text
    func showTranslatedText(trans:[TranslateLanguage]){
        self.translate = trans
        for item in translate{
        //self.translation = item.vText
        self.translatedTextLabel.text = item.vText
        }
    }
    
    // a small struct for the links
    private struct storyboard{
        //yandex link
        static let yandexURL = "https://translate.yandex.net"
        //api key
        static let apiKey = "key=trnsl.1.1.20160824T103232Z.7bb413dee8576be4.706c74de4629eb49f8179356309b690e4deb172d"
        //translate link
        static let apiURLTranslate = "/api/v1.5/tr.json/translate?"
        //get languages link
        static let apiUrlGetLang = "/api/v1.5/tr.json/getLangs?ui=en&"
    }
    
    func showDataGetLangs(getLangs:[SupportedLangs]){// show and set the supported languages
        self.getLan = getLangs
        let array = NSMutableArray()
        for item in getLan {
            for languages in item.vLangs.values {
                
                array.addObject(languages)
            }
//            for codes in item.vLangs.keys{
//                print(codes)
//            }
        }
        let arr = array as AnyObject as! [String] // order the Array and put it into pickerarray
        let sortedArray = arr.sort { $0.localizedCaseInsensitiveCompare($1) == NSComparisonResult.OrderedAscending }
        pickerArray = sortedArray
    }
    
    @IBAction func translateAction(sender: AnyObject) {// translate button. Get the links, order it, call the api
        let translateFromString = self.translateFrom.text
        let translateToString = self.translateTo.text
        let textToTranslate = "text=" + textToTranslateTextField.text!
        let dict = NSMutableDictionary()
        for item in getLan{// create dictionary from the class for supported languages
            dict.addEntriesFromDictionary(item.vLangs)
            }
        
        let translationDirectionWay = getKeyFromValue(translateFromString!, translateTo: translateToString!, dictionaryToGetKey: dict)
        
        let translationDirectionLink = "lang=" + translationDirectionWay
        
        let translationCondition = isSupported(translationDirectionWay)

        //Translate final link
        let FinalUrlTranslate = storyboard.yandexURL + storyboard.apiURLTranslate + translationDirectionLink + "&" + storyboard.apiKey + "&" + textToTranslate
        
        if translationCondition == true {
            callApiTranslateText(FinalUrlTranslate)
        }else{
            callApiTranslateText(FinalUrlTranslate)
            print("This Direction isnt included in yandex api supported direction. But for some directions Yandex does the translation! Cheers!")
        }
        
        
    }
    // method if language supported by Yandex
    func isSupported(translationDirection:String) -> Bool {
        var isSupp = Bool()
        for item in getLan {
            for direction in item.vDirs {
                print(direction)
                if translationDirection == direction as! String {
                    isSupp = true
                    return isSupp
                }else {
                    isSupp = false
                }
            }
        }
        return isSupp
    }
    
    // Method to get the key from the values of supported languages to clerify the link
    func getKeyFromValue(translateFrom:String, translateTo:String, dictionaryToGetKey:NSMutableDictionary) -> String {// key den value yu bulma Translation Direction için
        var FinalString = ""
        var translateFromCode = ""
        var translateToCode = ""
        
        if translateFrom != "" && translateTo != ""{
            for  key in dictionaryToGetKey.allKeysForObject(translateFrom){
            translateFromCode = key as! String
            }
            for  key in dictionaryToGetKey.allKeysForObject(translateTo){
            translateToCode = key as! String
        }
    }
        FinalString = translateFromCode + "-" + translateToCode
        return FinalString
    }
    
    //PickerView Methods
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int  {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 0 {
            return pickerArray.count
        }else if pickerView.tag == 1{
            return pickerArray.count
        }
        return 1
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 0 {
            return pickerArray[row] as? String
        }else if pickerView.tag == 1{
            return pickerArray[row] as? String
        }
        return ""
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)  {
        if pickerView.tag == 0 {
            translateFrom.text = pickerArray[row] as? String
        }else if pickerView.tag == 1{
            translateTo.text = pickerArray[row] as? String
        }
    }
    
}

    