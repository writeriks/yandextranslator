//
//  APIManager.swift
//  yandexTranslator
//
//  Created by Emir haktan Ozturk on 24/08/16.
//  Copyright © 2016 emirhaktan. All rights reserved.
//

import Foundation

class APIManager {

    func translateText(urlString:String, completion:[TranslateLanguage] -> Void){
    
        let config = NSURLSessionConfiguration.ephemeralSessionConfiguration()
        
        let session = NSURLSession(configuration: config)
        
        let url = NSURL(string: urlString)!  // url i al
        
        let task = session.dataTaskWithURL(url) { // url ile task oluştur içinde data, response ve error olsun
            (data, response, error) -> Void in
            
            if error != nil {
                
                print(error!.localizedDescription)
                
            }else{

                do{
                    if let json = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments) as? JSONDictionary {
                        
                        //print(json)
                        
                        var translate = [TranslateLanguage]()
                        /*
                         
                         Eğer json direk geldiyse sadece json'ın içinde başka bir obje yoksa direk aşağıdaki şekilde atılır
                         
                         */
                        let entry = TranslateLanguage(data: json)
                        translate.append(entry)
                        

                        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
                        dispatch_async(dispatch_get_global_queue(priority,0)){
                            dispatch_async(dispatch_get_main_queue()){
                                completion(translate)
                            }
                        }

                    }
                    
                }catch{
                    print("error in Json Serialization")
                }
            }
        
        }
    task.resume()
    }

    
    func getLangs(urlString:String, completion:[SupportedLangs] -> Void){
        
        let config = NSURLSessionConfiguration.ephemeralSessionConfiguration()
        let session = NSURLSession(configuration: config)
        let url = NSURL(string: urlString)!  // url i al
        let task = session.dataTaskWithURL(url) { // url ile task oluştur içinde data, response ve error olsun
            (data, response, error) -> Void in
        
            if error != nil {
                
                print(error!.localizedDescription)
                
            }else{
            
                do{
                    
                if let json = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments) as? JSONDictionary {
                
                    
                    var getLan = [SupportedLangs]()
                    let entry = SupportedLangs(data: json)
                    getLan.append(entry)
                    
                    let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
                    dispatch_async(dispatch_get_global_queue(priority,0)){
                        dispatch_async(dispatch_get_main_queue()){
                            completion(getLan)
                        }
                    }
                }
            }catch{
                print("error in Json Serialization")
                }
            }
        }
        task.resume()
    }
}

