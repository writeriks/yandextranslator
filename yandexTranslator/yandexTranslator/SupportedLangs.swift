//
//  SupportedLangs.swift
//  yandexTranslator
//
//  Created by Emir haktan Ozturk on 31/08/16.
//  Copyright © 2016 emirhaktan. All rights reserved.
//

import Foundation
class SupportedLangs {

    private var _vDirs:JSONArray
    private var _vLangs:JSONDictionary
    
    var vDirs:JSONArray{
    return _vDirs
    }
    
    var vLangs:JSONDictionary{
    return _vLangs
    }
    
    init(data:JSONDictionary){
    
        if let  vDirs = data["dirs"] as? JSONArray{
            self._vDirs = vDirs
        }else{
            self._vDirs = []
        }
        
        if let vLangs = data["langs"] as?JSONDictionary{
            self._vLangs = vLangs
        }else{
            self._vLangs = [:]
        }
        
    }
}