//
//  Constraints.swift
//  yandexTranslator
//
//  Created by Emir haktan Ozturk on 24/08/16.
//  Copyright © 2016 emirhaktan. All rights reserved.
//

import Foundation


typealias JSONDictionary = [String: AnyObject]

typealias JSONArray = Array<AnyObject>

let WIFI = "WIFI Available"

let NOACCESS = "No Internet Access"

let WWAN = "Cellular Access Available"

let ydxURL = "https://translate.yandex.net"