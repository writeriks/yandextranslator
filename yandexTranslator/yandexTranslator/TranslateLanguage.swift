//
//  TranslateLanguage.swift
//  yandexTranslator
//
//  Created by Emir haktan Ozturk on 24/08/16.
//  Copyright © 2016 emirhaktan. All rights reserved.
//

import Foundation

class TranslateLanguage {

    private var _vCode:Int
    private var _vLang:String
    private var _vText:String

    
    var vCode:Int{
        return _vCode
    }
    
    var vLang: String{
        return _vLang
    }
    
    var vText: String{
        return _vText
    }
    
    
    init(data:JSONDictionary){

        
        if let vLang = data["lang"] as? String{
            self._vLang = vLang
        }else{
        self._vLang = ""
        }
        if let text = data["text"] as? JSONArray,
            let vText = text[0] as? String{
            self._vText = vText
        }else{
        self._vText = ""
        }
        
        if let vCode = data["code"] as? Int{
            self._vCode = vCode
        }else{
            self._vCode = 0
        }
    }
        
}
    


    
